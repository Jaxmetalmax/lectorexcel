﻿using System;
using System.Configuration;
using System.Data;
using System.Windows.Forms;
using LectorExcel.Properties;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.SqlClient;
using Npgsql;
using System.Diagnostics;


namespace LectorExcel
{
    public partial class frmMain : Form
    {
        //SqlConnection oCon;
        NpgsqlConnection opCon;
        //Process p = new Process();

        public frmMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string cFilePath = "";
                Excel.Application xlApp;
                Excel.Workbook xlWorkbook;
                Excel.Worksheet xlSheet;
                Excel.Range xlRango;
                var misValue = Type.Missing;
               

                cFilePath = openFileDialog1.FileName;

                xlApp = new Excel.ApplicationClass();

                xlWorkbook = xlApp.Workbooks.Open(cFilePath, misValue, misValue,

                misValue, misValue, misValue, misValue, misValue, misValue,

                misValue, misValue, misValue, misValue, misValue, misValue);

                // seleccion de la hoja de calculo
                // get_item() devuelve object y numera las hojas a partir de 1

                for (int pag = 26; pag <= 32;pag++ )
                {
                    xlSheet = (Excel.Worksheet)xlWorkbook.Worksheets.get_Item(pag);

                    xlRango = xlSheet.UsedRange;

                    int xlRows = xlRango.Rows.Count;
                    int xlCols = xlRango.Columns.Count;

                    string cFields = "";

                    for (int col = 1; col <= xlCols; col++)
                    {
                        string xlCelda = "";

                        if ((xlRango.Cells[1, col] as Excel.Range).Value2 != null &&
                            (xlRango.Cells[1, col] as Excel.Range).Value2.ToString().Trim() != "")
                        {
                            xlCelda = (xlRango.Cells[1, col] as Excel.Range).Value2.ToString().Trim().Replace("'", "");
                            xlCelda = xlCelda.ToLower();
                        }
                        else
                        {
                            continue;
                        }
                        cFields += xlCelda + ",";
                    }

                    cFields = cFields.Substring(0, (cFields.Length - 1));
                    progressBar1.Maximum = xlRows - 2;

                    for (int row = 2; row <= xlRows; row++)
                    {
                        string cDatos = "";
                        string cQuery = "";

                        cQuery = "INSERT INTO " + comboBox1.SelectedValue.ToString() + " (" + cFields + ") VALUES (";

                        for (int col = 1; col <= xlCols; col++)
                        {
                            string xlCelda = "";

                            if ((xlRango.Cells[row, col] as Excel.Range).Value2 != null &&
                                (xlRango.Cells[row, col] as Excel.Range).Value2.ToString().Trim() != "")
                            {
                                xlCelda = (xlRango.Cells[row, col] as Excel.Range).Value2.ToString().Trim().Replace("'", "");

                            }
                            else
                            {
                                continue;
                            }

                            cDatos += "'" + xlCelda + "'" + ",";
                        }

                        try
                        {
                            cDatos = cDatos.Substring(0, (cDatos.Length - 1)) + ")";

                        }
                        catch (ArgumentOutOfRangeException ex)
                        {
                            break;
                            //throw;
                        }

                        cQuery = cQuery + cDatos;
                        /*
                        using (SqlCommand oCom = new SqlCommand(cQuery, oCon)) 
                        {
                            try
                            {
                                oCom.ExecuteNonQuery();
                            }
                            catch (SqlException err)
                            {
                                MessageBox.Show(err.Message + err.ErrorCode.ToString());
                            }
                        }*/

                        using (var npCom = new NpgsqlCommand(cQuery, opCon))
                        {
                            try
                            {
                                npCom.ExecuteNonQuery();
                                //progressBar1.PerformStep();
                            }
                            catch (NpgsqlException err)
                            {
                                MessageBox.Show(err.Message + " " + err.ErrorCode.ToString() + " Query: " + cQuery);
                            }
                        }

                    }
                }
                

                xlWorkbook.Close(false,misValue,misValue);
                xlApp.Quit();
                MessageBox.Show("Proceso terminado...");

                try
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
                    //System.Runtime.InteropServices.Marshal.ReleaseComObject(xlSheet);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlWorkbook);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se pudo liberar: " + ex.Message);
                    throw;
                }
                

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtHost.Text = Settings.Default.HOST;
            txtBDD.Text = Settings.Default.BDD;
            /*
            p.StartInfo.FileName = "plink.exe";
            p.StartInfo.Arguments = "-ssh -P 2221 -D 3333 -N -pw dasilm58 desarrollo1@dasi.no-ip.biz";
            p.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
            p.StartInfo.CreateNoWindow = true;
            p.Start();*/
                   
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            /*p.CloseMainWindow();
            p.Close();*/
            Settings.Default.HOST = txtHost.Text;
            Settings.Default.BDD = txtBDD.Text;
            Settings.Default.Save();

        }

        private void txtBDD_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBDD_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                var connstring = String.Format("Server={0};Port={1};" +
                                                  "User Id={2};Password={3};Database={4};",
                                                  txtHost.Text, "5434", "postgres",
                                                  "Dasilm58", txtBDD.Text);
                opCon = new NpgsqlConnection(connstring);
                opCon.Open();

                //Query para traer los nombres de las tablas, filtradas por nombre que comiencen por el prefijo "t_"
               // const string cQuery = "SELECT table_name FROM information_schema.tables where table_name @@ to_tsquery('t_') ORDER BY table_schema,table_name";
                const string cQuery = "SELECT table_name FROM information_schema.tables where table_schema='public' ORDER BY table_schema,table_name";
                using (var da = new NpgsqlDataAdapter(cQuery, opCon))
                {
                    var t = new DataTable();
                    da.Fill(t);
                    comboBox1.DataSource = t;
                    comboBox1.DisplayMember = "table_name";
                    comboBox1.ValueMember = "table_name";
                }
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
